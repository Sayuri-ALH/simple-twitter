package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		String Messageid = request.getParameter("messageid");

		if (!isValid1(Messageid, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		int messageId = Integer.parseInt(request.getParameter("messageid"));
		Message message = new MessageService().select(messageId);

		if (message == null) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	private boolean isValid1(String messageid, List<String> errorMessages) {

		String number;
		number = "^[0-9]+$";

		if ((!messageid.matches(number))) {
			errorMessages.add("不正なパラメーターが入力されました");
		} else if (StringUtils.isBlank(messageid)) {
			errorMessages.add("不正なパラメーターが入力されました");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		HttpSession session = request.getSession();
		String text = request.getParameter("text");
		if (!isValid(text, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		Message message = new Message();
		int id = (Integer.parseInt(request.getParameter("messageid")));
		message.setText(text);
		message.setId(id);

		new MessageService().update(message);
		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}